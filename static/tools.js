// Get the sub button
const btnSub = document.getElementById("btn_sub");
// Disable it
btnSub.disabled = true;


// The lenght of the siret number
const siretNb = 14;

// Get the altert message
let alert_danger = document.getElementById("alert_danger");
// Hide it
alert_danger.style.display = "none";

// Get the siret field
let siret_field = document.getElementById("siret_field");

// ----------------- SIRET FIELD CONTROLS ----------------- //
siret_field.addEventListener("keyup", function() {

    // Get the data without spaces
    let data = siret_field.value.replace(/\s/g,'');
    alert_danger.style.display = "none";

    if(data.length + 1 != 0){
        if(!containsOnlyNumbers(data) && !isLengthOk(data)){
            btnSub.disabled = true;
            showAlterDanger("Le numero ne doit pas contenir de lettres et contenir 14 chiffres 🤯");
        }else if(containsOnlyNumbers(data) && !isLengthOk(data)){
            btnSub.disabled = true;
            showAlterDanger("Le numéro ne fait pas 14 chiffres 🫤");
        }else if(!containsOnlyNumbers(data) && isLengthOk(data)){
            btnSub.disabled = true;
            showAlterDanger("Le numéro doit contenir uniquement des chiffres 😑");
        }else if(containsOnlyNumbers(data) && isLengthOk(data)){
            alert_danger.style.display = "none";
            btnSub.disabled = false;
        }
    }
});


function containsOnlyNumbers(str) {
    return /^\d+$/.test(str);
}

function isLengthOk(str){
    return str.length == siretNb;
}

function showAlterDanger(text){
    alert_danger.style.display = "flex";
    alert_danger.innerHTML = text;
}