function addContact() {
        var contactSection = document.createElement('div');
        contactSection.className = 'row';
        contactSection.innerHTML = `
            <div class="col-12 py-2">
                <hr class="my-4">
            </div>
            <div class="col-md-4 py-2">
                <label for="input_first_name_2" class="form-label">Prénom du contact </label>
                <input type="text" class="form-control" placeholder="Jean" name="first_name_field_2" id="input_first_name_2">
            </div>
            <div class="col-md-4 py-2">
                <label for="input_last_name_2" class="form-label">Nom du contact</label>
                <input type="text" class="form-control" placeholder="DUPONT" name="last_name_field_2" oninput="this.value = this.value.toUpperCase()" id="input_last_name_2">
            </div>
            <div class="col-md-4 py-2">
                <label for="input_phone_2" class="form-label">Téléphone du contact</label>
                <input type="text" class="form-control" placeholder="06 12 34 56 78" name="phone_field_2" maxlength=10 id="input_phone_2">
            </div>
            <div class="col-md-6 py-2">
                <label for="input_role_2" class="form-label">Fonction du contact</label>
                <input type="text" class="form-control" placeholder="Responsable d'équipe" name="role_field_2" id="input_role_2">
            </div>
            <div class="col-md-6 py-2">
                <label for="input_mail_2" class="form-label">Mail du contact</label>
                <input type="email" class="form-control" placeholder="jean.dupont@entreprise.com" name="mail_field_2" id="input_mail_2">
            </div>`;
        var contact2Div = document.querySelector('.contact-2');
        contact2Div.appendChild(contactSection);
        // Hide the add button
        document.getElementById('add_button').style.display = 'none';
    }

    // Listener on the submit button to chekc the optionnal fields
    document.getElementById('siret_form').addEventListener('submit', function(event) {
      // If there is at least one value in the second contact fields, check if all the fields are filled
      const all_second_fields = document.querySelectorAll('[id$="_2"]');
      let all_second_fields_filled = false;

      for (var i = 0; i < all_second_fields.length; i++) {
          if (all_second_fields[i].value.replace(/\s/g, '') !== "") {
            all_second_fields_filled = true;
            break;
          }
      }

      if(all_second_fields_filled){
        for (var i = 0; i < all_second_fields.length; i++) {
          if (all_second_fields[i].value.replace(/\s/g, '') === "") {
            // Show the alert message if it is not already displayed
            if (document.querySelector('.alert-danger') === null) {
                var message = document.createElement('div');
                message.className = 'alert alert-danger my-2 text-center';
                message.setAttribute('role', 'alert');
                message.innerHTML = '⚠️ Veuillez remplir tous les champs du deuxième contact';

                var contact2Div = document.querySelector('.contact-2');
                contact2Div.appendChild(message);
            }
            // Stop the submit
            event.preventDefault();
            break;
          }
        }
      } else {
          // If there is no value in the second contact fields, remove the alert message if it is displayed
          // And disable the second contact fields
            if (document.querySelector('.alert-danger') !== null) {
                document.querySelector('.alert-danger').remove();
            }
            for (var i = 0; i < all_second_fields.length; i++) {
                all_second_fields[i].setAttribute('disabled', true);
            }
      }
    });