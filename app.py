import os
from pprint import pprint

from flask import Flask, render_template, request, redirect, flash
from model.model import DataManagement

app = Flask(__name__)
app.secret_key = b'_5#y2L"F4Q8z\n\xec]/'

# Getting the base path of the Flask project
basedir = os.path.abspath(os.path.dirname(__file__))
# Resource directory
resource_dir = os.path.join(basedir, 'Resources')

model = DataManagement(resource_dir)


@app.route("/")
def index():
    return render_template("index.html", isOk=True)


@app.route("/company", methods=["POST"])
def company():
    """ This route represents the check for the company siret"""
    is_already_in_file = False

    # Get the siret from user
    siret = request.form.to_dict()["siret_field_value"].replace(" ", "")

    # First verification : only letters and size of 14
    conform = test_siret_number(siret)
    if not conform:
        flash("Le numéro n'est pas conforme, il doit faire 14 caractères et ne contenir que des chiffres 😠", "danger")
        return redirect("/")

    # Chekc if there are two contacts for this company
    if model.is_two_contacts(siret):
        flash("Il y a déjà deux contacts pour cette entreprise, veuillez en choisir une autre 😎", "danger")
        return redirect("/")

    # Second verification : Check if there is at least one contact
    if model.is_siret_number_exists_in_file(siret):
        is_already_in_file = True

    # Third verification : Check if the Siret number exists in the insee DB  - 43210419800077
    data = model.get_data_from_api(siret, is_already_in_file)
    if not data:
        flash("Ce Siret n'existe pas ou n'as pas été trouvé, veuillez en choisir un autre 😬", "danger")
        return redirect("/")

    # Set the data to the model instance
    model.current_data = data

    return render_template("form.html", data=data, siret=siret)


@app.route("/write_data", methods=["POST"])
def write_data():
    """ This method receives the data from the user : API data company + the contact information """
    args = request.form.to_dict()
    all_data = model.current_data

    # Add the args to the current model
    all_data.update(args)

    # Save the data
    try:
        model.save_data_in_excel(all_data)
        flash("Vos informations ont été envoyées avec succès ! 😄🎉", "success")
        model.current_data = {}
    except Exception as e:
        flash("Erreur lors l'insertion des données, veuillez rééssayer 🥸", "danger")
        print("Error during the data insertion")
        print(e)

    return redirect("/")


@app.route("/admin", methods=["GET"])
def admin():
    """ Troll page for dumbs """
    return '<img src="static/meme.jpg" width=75%>'


def test_siret_number(s):
    """ Test if the string in args is correctly formatted for a siret number """
    try:
        return len(s) == 14 and s.isnumeric()
    except Exception as e:
        return False
