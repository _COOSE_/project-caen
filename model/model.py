import pandas as pd
from api_insee import ApiInsee
import os



##### CREDENTIALS #####
# API Key
API_KEY = "zq8FyfkyTchT8XH6K2hpCD8wQLQa"
# API SECRET
API_SECRET = "uigPNkan1ebPN8FpVxLv4r6lkOwa"

# This dict represent the correspondance between the effectif tranche and the real number
# Because the API provide the effectif tranche and not the real number
# Example :
# tranche 01 -> 1-2 employees
# tranche 02 -> 3-5 employees
# ...
# tranche 21 -> 50-99 employees
# ...
CORRESPONDANCE_TRANCHE_EFFECTIF = {"00": "0", "01": "1 ou 2", "02": "3-5", "03": "6-9", "11": "10-19", "12": "20-49",
                                   "21": "50-99", "22": "100-199", "31": "200-249", "32": "250-499", "41": "500-999",
                                   "42": "1000-1999", "51": "2000-4999", "52": "5000-9999", "53": "10000 et plus"}

CORRESPONDANCE_TYPE_VOIE = {"AV": "Avenue", "BD": "Boulevard", "CHE": "Chemin", "CR": "Cours", "DOM": "Domaine",
                            "IMP": "Impasse", "RUE": "Rue", "PL": "Place", "QUAI": "Quai", "RES": "Résidence",
                            "RTE": "Route", "TR": "Traverse", "VOIE": "Voie", "ALL": "Allée"}
# The file path where we want to save the data
OUT_FILE = "out.xlsx"

EXCEL_FIELDS = [
    "Siret",
    "Raison sociale",
    "Code APE",
    "Adresse",
    "Code Postal",
    "Ville",
    "Nombre de salariés",
    "Nom du contact",
    "Prénom du contact",
    "Fonction du contact",
    "Téléphone du contact",
    "Adresse mail du contact",
    "Nom du contact 2",
    "Prénom du contact 2",
    "Fonction du contact 2",
    "Téléphone du contact 2",
    "Adresse mail du contact 2",
    "Prénom étudiant",
    "Nom étudiant",
    "Promo étudiant"
]


def get_data_from_company(company_siret):
    """ Get the information from the API based in the SIRET number. Return only the field we wwant
    Args:
        company_siret (str): SIRET number of the company
    Returns:
        dict: Information about the company
    """
    # Select only the field we want
    fields = [
        "denominationUniteLegale",
        "activitePrincipaleUniteLegale",
        "trancheEffectifsEtablissement",
        "libelleCommuneEtablissement",
        "codePostalEtablissement",
        "typeVoieEtablissement",
        "libelleVoieEtablissement",
        "numeroVoieEtablissement"
    ]
    # Call the API
    api = ApiInsee(
        key=API_KEY,
        secret=API_SECRET
    )
    try:
        data = api.siret(company_siret, champs=fields).get()["etablissement"]
    except Exception as e:
        return None

    # Get the real number of employees
    try:
        data["trancheEffectifsEtablissement"] = CORRESPONDANCE_TRANCHE_EFFECTIF[data["trancheEffectifsEtablissement"]]
    except KeyError:
        data["trancheEffectifsEtablissement"] = ""

    # Get the real type of the street
    try:
        data['adresseEtablissement']['typeVoieEtablissement'] = CORRESPONDANCE_TYPE_VOIE[
            data['adresseEtablissement']['typeVoieEtablissement']]
    except KeyError:
        data['adresseEtablissement']['typeVoieEtablissement'] = ""

    # Prevent from the None value
    for key, value in data.items():
        # if the value is a dict
        if isinstance(value, dict):
            for key2, value2 in value.items():
                if value2 is None:
                    value[key2] = ""
        else:
            if value is None:
                data[key] = ""
    return data


class DataManagement:
    """ this class represents the API calls to insee and all data management about it

     Read an Excel file and fill it with data from an API (INSEE)
        API :  https://api.insee.fr/catalogue/site/themes/wso2/subthemes/insee/pages/item-info.jag?name=Sirene&version=V3&provider=insee
        api-insee : https://github.com/ln-nicolas/api_insee
    """

    def __init__(self, resource_dir):
        """ Init method """
        self.current_data = {}
        self.out_file_path = os.path.join(resource_dir, OUT_FILE)


    def is_siret_number_exists_in_file(self, siret):
        """ Check in the Excel file if the siret number already exists
        :param siret: The siret number we want to get data
        :return boolean: True if the siret number is already in the file False else
        """
        # Get the df
        if not os.path.exists(self.out_file_path):
            return False

        try:
            df_data = pd.read_excel(self.out_file_path, dtype=str, engine='openpyxl')
            return siret in df_data["Siret"].values
        except Exception as ex:
            print(f"Error while opening the file : {ex}")
            return False

    def is_two_contacts(self, siret):
        """
        Check if there is already two contacts for this company
        :param siret: The siret number we want to get data
        :return: True if there is already two contacts, False else
        """
        # Get the df
        if not os.path.exists(self.out_file_path):
            return False

        try:
            df_data = pd.read_excel(self.out_file_path, dtype=str, engine='openpyxl')
            df_data = df_data[df_data["Siret"] == siret]

            # IF the df is not empty
            if not df_data.empty:
                # Get the first line
                c1 = df_data["Nom du contact"].values[0]
                c2 = df_data["Nom du contact 2"].values[0]
                if (str(c1) != "nan") and (str(c2) != "nan"):
                    return True
                else:
                    return False
        except Exception as ex:
            print(f"Error while opening the file : {ex}")
            return False

    def get_data_from_api(self, siret, is_already_in_file):
        """ Get the data from a company based on the siret name

        :param siret: The siret number we want to get data
        :param is_already_in_file: True if the siret number is already in the file False else
        :return: dict if there is a company wth this siret number, else None
        """
        try:
            company_data = get_data_from_company(siret)

            if is_already_in_file:
                # get the data of the contact(s) from the file
                contact_data = self.get_contact_data_from_file(siret)
                if contact_data:
                    company_data.update(contact_data)

            return company_data
        except Exception as ex:
            print("Error during the company data retrieve")
            print(ex)
            return None


    def get_contact_data_from_file(self, siret):
        """ Get the contact data from the file based on the siret number

        :param siret: The siret number we want to get data
        :return: dict if there is a company wth this siret number, else None
        """
        res = {}
        try:
            df_data = pd.read_excel(self.out_file_path, dtype=str, engine='openpyxl')
            df_data = df_data[df_data["Siret"] == siret]

            # Get the data of the contact(s) from the file

            # IF the df is not empty
            if not df_data.empty:
                # Get the first line
                res["last_name_field"] = df_data["Nom du contact"].values[0]
                res["first_name_field"] = df_data["Prénom du contact"].values[0]
                res["role_field"] = df_data["Fonction du contact"].values[0]
                res["phone_field"] = df_data["Téléphone du contact"].values[0]
                res["mail_field"] = df_data["Adresse mail du contact"].values[0]

            return res
        except Exception as ex:
            print("Error during the contact data retrieve")
            print(ex)
            return None


    def save_data_in_excel(self, data):
        """ Save the data took from the company and save it in the file

        :param data (dict) the data we want to insert in the file
        """

        # Get the df
        if os.path.exists(self.out_file_path):
            # read the Excel file
            try:
                out_df = pd.read_excel(self.out_file_path, dtype=str)
            except Exception as e:
                print(e)
                raise Exception("Error during the Excel reading")
        else:
            # create it
            out_df = pd.DataFrame(columns=EXCEL_FIELDS)

        # Reformat the data and prepare the dict to be insered
        dict_to_insert = {
            "Siret": data["siret_field_value"],
            "Raison sociale": data["uniteLegale"]["denominationUniteLegale"],
            "Code APE": data["uniteLegale"]["activitePrincipaleUniteLegale"],
            "Adresse": f"{data['adresseEtablissement']['numeroVoieEtablissement']} {data['adresseEtablissement']['typeVoieEtablissement']} {data['adresseEtablissement']['libelleVoieEtablissement']}",
            "Code Postal": data["adresseEtablissement"]["codePostalEtablissement"],
            "Ville": data["adresseEtablissement"]["libelleCommuneEtablissement"],
            "Nombre de salariés": data["trancheEffectifsEtablissement"],
            "Nom du contact": data["last_name_field"],
            "Prénom du contact": data["first_name_field"],
            "Téléphone du contact": data["phone_field"],
            "Adresse mail du contact" : data["mail_field"],
            "Prénom étudiant" : data["student_name_field"],
            "Nom étudiant" : data["student_lastname_field"],
            "Promo étudiant" : data["student_promo_field"],
            "Fonction du contact" : data["role_field"]
        }

        # Add the contact 2 if it exists
        if "last_name_field_2" in data.keys():
            dict_to_insert["Nom du contact 2"] = data["last_name_field_2"]
            dict_to_insert["Prénom du contact 2"] = data["first_name_field_2"]
            dict_to_insert["Téléphone du contact 2"] = data["phone_field_2"]
            dict_to_insert["Adresse mail du contact 2"] = data["mail_field_2"]
            dict_to_insert["Fonction du contact 2"] = data["role_field_2"]


        # Delete the row in the df if the siret already exists
        if self.is_siret_number_exists_in_file(data["siret_field_value"]):
            out_df = out_df[out_df["Siret"] != data["siret_field_value"]]

        # update the Excel by export the df
        try:
            df_from_dict = pd.DataFrame([dict_to_insert])
            out_df = pd.concat([out_df, df_from_dict], ignore_index=True)
            out_df.to_excel(self.out_file_path, index=False)
        except Exception as e:
            print(e)
            raise Exception("Error during the Excel export")
